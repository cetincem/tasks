import {action, computed, makeObservable, observable} from 'mobx';
import Task from './models/Task';

class Store {
  tasks: Task[] = [];

  constructor() {
    makeObservable(this, {
      tasks: observable,
      tasksCount: computed,
      activeTasksCount: computed,
      completedTasksCount: computed,
      taskById: computed,
      activeTasks: computed,
      completedTasks: computed,
      addTask: action,
      deleteTask: action,
      completeTask: action,
      updateTask: action,
    });

    // TODO: delete sample data
    this.tasks.push(new Task('task 1', 'task 1 description'));
    this.tasks.push(new Task('task 2', 'task 2 description'));
    this.tasks.push(new Task('task 3', 'task 3 description'));

    this.completeTask(this.tasks[2].getId());
  }

  // returns the number of all tasks
  get tasksCount() {
    return this.tasks.length;
  }

  // returns the number of tasks that are not completed
  get activeTasksCount() {
    return this.tasks.filter(task => !task.isCompleted()).length;
  }

  // returns the number of tasks that are completed
  get completedTasksCount() {
    return this.tasks.filter(task => task.isCompleted()).length;
  }

  get taskById() {
    return (id: string) => this.tasks.find(task => task.getId() === id);
  }

  // returns the tasks that are not completed
  get activeTasks() {
    return this.tasks.filter(task => !task.isCompleted());
  }

  // returns the tasks that are completed
  get completedTasks() {
    return this.tasks.filter(task => task.isCompleted());
  }

  addTask = (task: Task) => {
    this.tasks.push(task);
  };

  deleteTask = (id: string) => {
    this.tasks = this.tasks.filter(task => task.getId() !== id);
  };

  completeTask = (id: string) => {
    this.tasks.filter(task => task.getId() === id)[0].complete();
  };

  updateTask = (id: string, title: string, description: string) => {
    this.tasks.filter(task => task.getId() === id)[0].update(title, description);
  }
}

export default Store;
