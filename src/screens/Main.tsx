import React, {useState} from 'react';
import {BottomNavigation} from 'react-native-paper';
import ActiveTasks from '../components/ActiveTasks';
import CompletedTasks from '../components/CompletedTasks';

const MainScreen: React.FC = () => {
  const [index, setIndex] = useState(0); // bottom navigation index

  // bottom navigation routes
  const [routes] = useState([
    {key: 'activeTasks', title: 'Active Tasks'},
    {key: 'completedTasks', title: 'Completed Tasks'},
  ]);

  const appScene = BottomNavigation.SceneMap({
    activeTasks: ActiveTasks,
    completedTasks: CompletedTasks,
  });

  return (
    <BottomNavigation
      navigationState={{index, routes}}
      onIndexChange={setIndex}
      renderScene={appScene}
    />
  );
};

export default MainScreen;
