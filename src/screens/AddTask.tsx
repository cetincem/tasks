import { useNavigation } from '@react-navigation/native';
import { inject, observer } from 'mobx-react';
import React, { useState } from 'react';
import {StyleSheet, View} from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import Task from '../models/Task';
import Store from '../store';

interface Props {
  store?: Store;
}

const AddTaskScreen: React.FC<Props> = ({store}) => {
  const navigation = useNavigation();

  const [title, setTitle] = useState(''); // title state
  const [description, setDescription] = useState(''); // description state
  
  const submit = () => {
    // check title
    if (title.trim().length === 0) {
      // TODO: show warning
      return;
    }

    // check description
    if (description.trim().length === 0) {
      // TODO: show warning
      return;
    }

    // create new task object
    const task: Task = new Task(title, description);
    store?.addTask(task); // add the task to the store
    // TODO: inform the user
    navigation.goBack();
  };

  const cancel = () => {
    navigation.goBack();
  }
  
  return (
    <View style={styles.container}>
      <TextInput
        label="Title"
        value={title}
        onChangeText={text => setTitle(text)}
        mode="outlined"
      />
      <TextInput
        label="Description"
        value={description}
        onChangeText={text => setDescription(text)}
        mode="outlined"
      />
      <View style={styles.buttonsContainer}>
        <Button style={styles.submitButton} mode="contained" onPress={submit}>
          Create
        </Button>
        <Button mode="outlined" onPress={cancel}>
          Cancel
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  buttonsContainer: {
    flexDirection: 'row',
    paddingTop: 16,
  },
  submitButton: {
    marginRight: 8,
  },
});

export default inject('store')(observer(AddTaskScreen));
