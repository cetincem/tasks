import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import {inject, observer} from 'mobx-react';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Headline, Text, TextInput} from 'react-native-paper';
import StackNavigatorParamList from '../interfaces/StackNavigatorParamList';
import Store from '../store';

type _RouteProp = RouteProp<StackNavigatorParamList, 'TaskDetail'>;

interface Props {
  route: _RouteProp;
  store?: Store;
}

const TaskDetailScreen: React.FC<Props> = ({route, store}: Props) => {
  const navigation = useNavigation();

  const [isEditable, setEditable] = useState(true);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  // get taskId from route params
  const taskId = route.params?.taskId;

  // if taskId route param is not defined, go back to Main screen
  if (!taskId) {
    navigation.goBack();
  }

  // get the task from the store by taskId
  const task = store?.taskById(taskId);

  useEffect(() => {
    if (task) {
      setTitle(task.getTitle());
      setDescription(task.getDescription());

      if (task.isCompleted()) {
        setEditable(false);
      }
    }
  }, [task]);

  const onComplete = () => {
    store?.completeTask(taskId);
    setEditable(false);
  };

  const deleteTask = (taskId: string) => {
    if (store) {
      store.deleteTask(taskId);
      navigation.goBack();
    }
  };

  const updateTask = () => {
    store?.updateTask(taskId, title, description);
    navigation.goBack();
  };

  // if task is not found, show error message
  if (!task) {
    return <TaskNotFound />;
  }

  return (
    <View style={styles.container}>
      {isEditable === true && (
        <View style={styles.section}>
          <TextInput
            label="Title"
            value={title}
            mode="outlined"
            onChange={e => setTitle(e.nativeEvent.text)}
          />
          <TextInput
            label="Description"
            value={description}
            mode="outlined"
            multiline={true}
            numberOfLines={5}
            style={styles.descriptionInput}
            onChange={e => setDescription(e.nativeEvent.text)}
          />
        </View>
      )}

      {isEditable === false && (
        <View style={styles.section}>
          <Headline>{task.getTitle()}</Headline>
          <Text>{task.getDescription()}</Text>
        </View>
      )}

      <Text>Created at: {task.whenCreated().toUTCString()}</Text>

      {/* if task is completed show when it was */}
      {task.isCompleted() === true && (
        <View style={styles.section}>
          <Text>Completed at: {task.whenCompleted()?.toUTCString()}</Text>
        </View>
      )}

      {/* if task is not completed show complete button */}
      {task.isCompleted() === false && (
        <View>
          <Button
            style={styles.updateButton}
            mode="contained"
            onPress={updateTask}>
            Update
          </Button>

          <Button
            style={styles.completeButton}
            mode="contained"
            onPress={onComplete}>
            Complete
          </Button>
        </View>
      )}

      <Button mode="contained" onPress={() => deleteTask(taskId)}>
        Delete
      </Button>
    </View>
  );
};

const TaskNotFound: React.FC = () => {
  return (
    <View>
      <Text>Task not found!</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  section: {
    marginBottom: 16,
  },
  descriptionInput: {
    marginTop: 16,
    height: 100,
    textAlignVertical: 'top',
  },
  updateButton: {
    marginTop: 16,
  },
  completeButton: {
    marginTop: 16,
    marginBottom: 16,
  },
});

export default inject('store')(observer(TaskDetailScreen));
