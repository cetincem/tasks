import {useNavigation} from '@react-navigation/native';
import {inject, observer} from 'mobx-react';
import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {Divider, List, Text} from 'react-native-paper';
import Task from '../models/Task';
import Store from '../store';

interface Props {
  store?: Store;
}

const CompletedTasks: React.FC<Props> = ({store}) => {
  const navigation = useNavigation();

  const showTaskDetail = (task: Task) => {
    navigation.navigate('TaskDetail' as never, {taskId: task.getId()} as never);
  };

  // if there is no completed task, show empty view
  if (store?.completedTasks.length === 0) {
    return <EmptyView />;
  }

  return (
    <SafeAreaView>
      <List.Section>
        {store?.completedTasks.map((task: Task) => {
          return (
            <View key={task.getId()}>
              <List.Item
                title={task.getTitle()}
                description={task.getDescription()}
                onPress={() => showTaskDetail(task)}
              />
              <Divider />
            </View>
          );
        })}
      </List.Section>
    </SafeAreaView>
  );
};

const EmptyView: React.FC = () => {
  return (
    <View style={emptyViewStyles.container}>
      <Text>There is no completed task yet!</Text>
    </View>
  );
};

const emptyViewStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default inject('store')(observer(CompletedTasks));
