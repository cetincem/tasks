import {useNavigation} from '@react-navigation/native';
import {inject, observer} from 'mobx-react';
import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {Button, Divider, FAB, List, Text} from 'react-native-paper';
import Task from '../models/Task';
import Store from '../store';

interface Props {
  store?: Store;
}

const ActiveTasks: React.FC<Props> = ({store}) => {
  const navigation = useNavigation();

  const showTaskDetail = (task: Task) => {
    navigation.navigate('TaskDetail' as never, {taskId: task.getId()} as never);
  };

  if (store?.activeTasks.length === 0) {
    return <EmptyView />
  } else {
    return (
      <SafeAreaView style={styles.container}>
        <List.Section>
          {store?.activeTasks.map((task: Task) => {
            return (
              <View key={task.getId()}>
                <List.Item
                  title={task.getTitle()}
                  description={task.getDescription()}
                  onPress={() => showTaskDetail(task)}
                />
                <Divider />
              </View>
            );
          })}
        </List.Section>
        <FAB
          icon=""
          label="Add"
          style={styles.fab}
          onPress={() => navigation.navigate('AddTask' as never)}
        />
      </SafeAreaView>
    );
  }
};

const EmptyView: React.FC = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.emptyViewContainer}>
      <Text>There is no active task!</Text>
      <Button onPress={() => navigation.navigate('AddTask' as never)}>Add Task</Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  emptyViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default inject('store')(observer(ActiveTasks));
