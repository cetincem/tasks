import { extendObservable, observable } from "mobx";

export default class Task {
  private id: string;
  private title: string;
  private description: string;
  private completed: boolean;
  private createdAt: Date;
  private completedAt?: Date;

  constructor(title: string, description: string) {
    extendObservable(this, {
      id: observable,
      title: observable,
      description: observable,
      completed: observable,
      createdAt: observable,
      completedAt: observable,
    });
    
    this.id = `${Math.random().toString(16).slice(2)}-${Date.now().toString(
      16,
    )}`;
    this.title = title;
    this.description = description;
    this.completed = false;
    this.createdAt = new Date();
  }

  complete() {
    this.completed = true;
    this.completedAt = new Date();
  }

  getId() {
    return this.id;
  }

  getTitle() {
    return this.title;
  }

  setTitle(title: string) {
    this.title = title;
  }

  update(title: string, description: string) {
    this.title = title;
    this.description = description;
  }

  getDescription() {
    return this.description;
  }

  setDescription(description: string) {
    this.description = description;
  }

  isCompleted() {
    return this.completed;
  }

  whenCreated() {
    return this.createdAt;
  }

  whenCompleted() {
    return this.completedAt;
  }
}
