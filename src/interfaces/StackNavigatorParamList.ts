type StackNavigatorParamList = {
  Main: undefined;
  AddTask: undefined;
  TaskDetail: {
    taskId: string;
  };
};

export default StackNavigatorParamList;
