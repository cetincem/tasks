import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Provider} from 'mobx-react';
import React from 'react';
import StackNavigatorParamList from './src/interfaces/StackNavigatorParamList';
import AddTaskScreen from './src/screens/AddTask';

import MainScreen from './src/screens/Main';
import TaskDetailScreen from './src/screens/TaskDetail';
import Store from './src/store';

const store: Store = new Store();

const Stack = createNativeStackNavigator<StackNavigatorParamList>();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Main"
            component={MainScreen}
            options={{
              title: 'Tasks',
            }}
          />
          <Stack.Screen
            name="TaskDetail"
            component={TaskDetailScreen}
            options={{title: 'Task Detail'}}
          />
          <Stack.Screen
            name="AddTask"
            component={AddTaskScreen}
            options={{title: 'Add Task'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
