# Tasks
Sample react native project that keep track of tasks

## Used libraries
- react-native-paper
- react-navigation
- mobx

## How to run (iOS)
1. Clone the repo
2. Run `yarn` to install dependencies
3. Run `yarn ios` to start the app on iOS simulator

## How to run (Android)
1. Clone the repo
2. Run `yarn` to install dependencies
3. Run `yarn start` to start metro bundler
4. Run `yarn android` to start the app on Android emulator